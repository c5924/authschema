﻿using AuthSchema.Application.DTO.Response;
using AuthSchema.Domain.Model;
using System.Collections.Generic;

namespace AuthSchema.Application.Result
{
    public class LoginResult
    {
        public LoginStatus Status { get; set; }
        public string ErrorMessage { get; set; }
        public bool IsInvalid { get; set; }
        public IEnumerable<UserClienteResponse> UserClientes { get; set; }
        public IEnumerable<Product> Products { get; set; }
        public User User { get; set; }
        public TokenAuthentication AccessToken { get; set; }

        public LoginResult(LoginStatus status, string errorMessage, User user, TokenAuthentication accessToken, bool isInvalid, IEnumerable<UserClienteResponse> userClientes, IEnumerable<Product> products)
        {
            Status = status;
            ErrorMessage = errorMessage;
            User = user;
            IsInvalid = isInvalid;
            AccessToken = accessToken;
            UserClientes = userClientes;    
            Products = products;
        }

        public static LoginResult Succeed(User user, TokenAuthentication accessToken, IEnumerable<UserClienteResponse> userClientes, IEnumerable<Product> products) =>
            new LoginResult(LoginStatus.Succeed, string.Empty, user,accessToken, false, userClientes, products);
        public static LoginResult InvalidData(string message) =>
            new LoginResult(LoginStatus.InvalidData, message, null,null, true, null, null);

        public static LoginResult BlockedUser(User user, string message) =>
            new LoginResult(LoginStatus.BlockedUser, message, user, null, true, null, null);

        public static LoginResult Error(string message) =>
            new LoginResult(LoginStatus.Error, message, null, null, true, null, null);


        public static readonly LoginResult NoResult = new LoginResult(LoginStatus.NoResult, string.Empty, null, null, false, null, null);
    }
}
