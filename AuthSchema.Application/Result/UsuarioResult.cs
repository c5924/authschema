﻿using AuthSchema.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthSchema.Application.Result
{
    public class UsuarioResult
    {
        public UsuarioStatus Status { get; set; }
        public User User { get; set; }
        public string ErrorMessage { get; set; }

        public UsuarioResult()
        {

        }

        public static UsuarioResult Sucesso(User user) =>
            new UsuarioResult()
            {
                Status = UsuarioStatus.Sucesso,
                User = user,
            };
        public static UsuarioResult UsuarioNaoEncontrado() =>
            new UsuarioResult()
            {
                Status = UsuarioStatus.UsuarioNaoEncontrado,
                User = null,
                ErrorMessage = "user not found"
            };

        public static UsuarioResult DadosInvalidos(string mensagem) =>
            new UsuarioResult()
            {
                Status = UsuarioStatus.DadosInvalidos,
                User = null,
                ErrorMessage = mensagem
            };
        public static UsuarioResult ErroInterno(string mensagem) =>
            new UsuarioResult()
            {
                Status = UsuarioStatus.ErroInterno,
                User = null,
                ErrorMessage = mensagem
            };
    }
}
