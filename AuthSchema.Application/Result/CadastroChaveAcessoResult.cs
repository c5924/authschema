﻿using AuthSchema.Application.DTO.Response.Identificadores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthSchema.Application.Result
{
    public class CadastroChaveAcessoResult
    {
        public CadastroChaveAcessoStatus Status { get; set; }
        public IdentificadorUsuarioResponse Usuario { get; set; }
        public string ErrorMessage { get; set; }

        public CadastroChaveAcessoResult()
        {   }

        public static CadastroChaveAcessoResult Sucesso(int userId) => new CadastroChaveAcessoResult
        {
            Usuario = new IdentificadorUsuarioResponse(userId),
            Status = CadastroChaveAcessoStatus.Sucesso
        };

        public static CadastroChaveAcessoResult UsuarioNaoEncontrado() => new CadastroChaveAcessoResult
        {
            Usuario = null,
            ErrorMessage = "Usuário não encontrado",
            Status = CadastroChaveAcessoStatus.UsuarioNaoEncontrado
        };

        public static CadastroChaveAcessoResult ProdutoNaoEncontrado() => new CadastroChaveAcessoResult
        {
            Usuario = null,
            ErrorMessage = "Produto não encontrado.",
            Status = CadastroChaveAcessoStatus.ProdutoNaoEncontrado
        };

        public static CadastroChaveAcessoResult ErroInterno(string error) => new CadastroChaveAcessoResult
        {
            Usuario = null,
            ErrorMessage = error,
            Status = CadastroChaveAcessoStatus.ErroInterno
        };

        public static CadastroChaveAcessoResult DadosInvalidos(string error) => new CadastroChaveAcessoResult
        {
            Usuario = null,
            ErrorMessage = error,
            Status = CadastroChaveAcessoStatus.DadosInvalidos
        };

        public static CadastroChaveAcessoResult ChaveDeAcessoJaExistente() => new CadastroChaveAcessoResult
        {
            Usuario = null,
            ErrorMessage = "Já existe uma chave de acesso ativa informada para esse usuário no produto informado",
            Status = CadastroChaveAcessoStatus.ChaveDeAcessoJaExistente
        };

        public static CadastroChaveAcessoResult ChaveDeAcessoNaoEncontradaParaoUsuario() => new CadastroChaveAcessoResult
        {
            Usuario = null,
            ErrorMessage = "não existe uma chave de acesso informada para esse usuário no produto informado",
            Status = CadastroChaveAcessoStatus.ChaveDeAcessoNaoEncontradaParaoUsuario
        };

    }
}
