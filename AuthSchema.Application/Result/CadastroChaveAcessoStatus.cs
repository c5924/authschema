﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthSchema.Application.Result
{
    public enum CadastroChaveAcessoStatus
    {
        Sucesso,
        ProdutoNaoEncontrado,
        UsuarioNaoEncontrado,
        ChaveDeAcessoJaExistente,
        ChaveDeAcessoNaoEncontradaParaoUsuario,
        ErroInterno,
        DadosInvalidos,
    }
}
