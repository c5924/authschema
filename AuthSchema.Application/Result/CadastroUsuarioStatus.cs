﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthSchema.Application.Result
{
    public enum CadastroUsuarioStatus
    {
        DadosInvalidos,
        Sucesso,
        UsuarioJaExiste,
        ErroInterno
    }
}
