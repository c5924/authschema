﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthSchema.Application.Result
{
    public enum UsuarioStatus
    {
        Sucesso,
        UsuarioNaoEncontrado,
        DadosInvalidos,
        ErroInterno
    }
}
