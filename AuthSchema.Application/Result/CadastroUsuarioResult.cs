﻿using AuthSchema.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthSchema.Application.Result
{
    public class CadastroUsuarioResult
    {
        public CadastroUsuarioStatus Status { get; set; }
        public string ErrorMessage { get; set; }
        public User Usuario { get; set; }

        private CadastroUsuarioResult()
        {

        }

        public static CadastroUsuarioResult Sucesso(User user) => new CadastroUsuarioResult
        {
            Usuario = user,
            Status = CadastroUsuarioStatus.Sucesso
        };

        public static CadastroUsuarioResult DadosInvalidos(string error) => new CadastroUsuarioResult
        {
            Usuario = null,
            ErrorMessage = error,
            Status = CadastroUsuarioStatus.DadosInvalidos
        };
        public static CadastroUsuarioResult UsuariojaExiste(User user) => new CadastroUsuarioResult
        {
            Usuario = user,
            ErrorMessage = "usuário ja existe",
            Status = CadastroUsuarioStatus.UsuarioJaExiste
        };
        public static CadastroUsuarioResult ErroInterno(string error) => new CadastroUsuarioResult
        {
            Usuario = null,
            ErrorMessage = error,
            Status = CadastroUsuarioStatus.ErroInterno
        };
    }
}
