﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthSchema.Application.DTO.Request.Identificadores
{
    public class IdentificadorUsuarioRequest
    {
        public int _Id { get; set; }
    }
}
