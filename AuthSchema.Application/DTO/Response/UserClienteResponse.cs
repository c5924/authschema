﻿using AuthSchema.Domain.Model.CardapioVirtual;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthSchema.Application.DTO.Response
{
    public class UserClienteResponse
    {
        public Cliente Cliente { get; set; }
        public TipoAcesso TipoAcesso { get; set; }
        public TipoPessoa TipoPessoa { get; set;}
        public UserCliente UserCliente { get; set; }
    }
}
