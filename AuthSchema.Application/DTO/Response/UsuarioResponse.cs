﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthSchema.Application.DTO.Response
{
    public class UsuarioResponse
    {
        public int _Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
}
