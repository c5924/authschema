﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthSchema.Application.DTO.Response.Identificadores
{
    public class IdentificadorUsuarioResponse
    {
        public int _Id { get; set; }
        public IdentificadorUsuarioResponse()
        {

        }

        public IdentificadorUsuarioResponse(int userId)
        {
            _Id = userId;
        }
    }

}
