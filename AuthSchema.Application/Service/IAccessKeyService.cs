﻿using AuthSchema.Application.Result;
using AuthSchema.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthSchema.Application.Service
{
    public interface IAccessKeyService
    {
        Task<CadastroChaveAcessoResult> CadastraChaveAcessoAsync(string password, int productId, int userId);
        Task<CadastroChaveAcessoResult> AlteraChaveAcessoAsync(int productId, int userId, bool ativo, bool bloqueado);
        Task<CadastroChaveAcessoResult> AlteraSenhaUsuarioAsync(string newPassword, int productId, int userId);
    }
}
