﻿using AuthSchema.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthSchema.Application.Service.LoginStrategy
{
    public interface ILoginProvider
    {
        IUserRepository UserRepository { get; }
        IAccessKeyRepository AccessKeyRepository { get; }
        IProductRepository ProductRepository{ get; }
        ICardapioVirtualRepository CardapioVirtualRepository{ get; }
    }
}
