﻿using System;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AuthSchema.Domain.Repository;

namespace AuthSchema.Application.Service.LoginStrategy
{
    public class LoginProvider : ILoginProvider
    {
        public IUserService UserService { get; set; }

        public IUserRepository UserRepository { get; set; }
        public IAccessKeyRepository AccessKeyRepository { get; set; }
        public IProductRepository ProductRepository { get; set; }
        public ICardapioVirtualRepository CardapioVirtualRepository { get; set; }

        public LoginProvider(IServiceProvider provider)
        {
            UserService = provider.GetRequiredService<IUserService>();
            UserRepository = provider.GetRequiredService<IUserRepository>();
            AccessKeyRepository = provider.GetRequiredService<IAccessKeyRepository>();
            ProductRepository = provider.GetRequiredService<IProductRepository>();
            CardapioVirtualRepository = provider.GetRequiredService<ICardapioVirtualRepository>();
        }
    }
}
