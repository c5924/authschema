﻿using AuthSchema.Application.Result;
using AuthSchema.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthSchema.Application.Service.LoginStrategy
{
    public interface ILoginStrategy
    {
        void Initialize(LoginParameters parameters);

        LoginParameters Parameters { get; }
    }

    public interface ILoginStrategy<TUser> : ILoginStrategy
        where TUser : User
    {
        Task<LoginResult> ExecuteLogin();

        Task<TokenAuthentication> RefreshUserToken(string key);

    }
}
