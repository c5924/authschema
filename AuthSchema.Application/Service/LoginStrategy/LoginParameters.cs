﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthSchema.Application.Service.LoginStrategy
{
    public class LoginParameters
    {
        public string Key { get; set; }
        public string Password { get; set; }
        public int ProductId { get; set; }
    }
}
