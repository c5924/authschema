﻿using AuthSchema.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthSchema.Application.Service.LoginStrategy
{
    public interface IUserLogin : ILoginStrategy<User>
    {
    }
}
