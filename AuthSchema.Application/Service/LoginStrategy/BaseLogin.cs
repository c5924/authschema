﻿using AuthSchema.Application.DTO.Response;
using AuthSchema.Application.Result;
using AuthSchema.Domain.Model;
using AuthSchema.Domain.Model.CardapioVirtual;
using AuthSchema.Infrastructure.Config;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Reflection.Metadata;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace AuthSchema.Application.Service.LoginStrategy
{
    public class BaseLogin<TUser> : ILoginStrategy<TUser>
        where TUser : User
    {
        protected readonly ILoginProvider _loginProvider;
        public LoginParameters Parameters { get; protected set; }
        public BaseLogin()
        {

        }

        public BaseLogin(ILoginProvider loginProvider)
        {
            _loginProvider = loginProvider;
        }

        public async Task<LoginResult> ExecuteLogin()
        {
            try
            {
                var user = await _loginProvider.UserRepository.GetUserByKeyAsync(Parameters.Key)
                    .ConfigureAwait(false);

                if (user == null)
                    return LoginResult.Error($"User not found in key: {Parameters.Key}");

                var accessKey = await _loginProvider.AccessKeyRepository.GetAccessKeyAsync(user.IdUsuario, Parameters.ProductId)
                    .ConfigureAwait(false);

                if (accessKey == null)
                    return LoginResult.InvalidData($"No access key found");

                var products = await _loginProvider.ProductRepository.GetProductsAsync(user.IdUsuario)
                    .ConfigureAwait(false);

                var passwordValidate = SecurityHandler.VerificarSenha(Parameters.Password, accessKey.AccessKeyAttr);

                if (!passwordValidate)
                    return LoginResult.InvalidData("invalid email or password");

                if (!accessKey.Active)
                    return LoginResult.BlockedUser(user, "your user is disabled");

                if (accessKey.Blocked)
                    return LoginResult.BlockedUser(user, "blocked User");

                var userProfile = await _loginProvider.UserRepository.GetUserProfilesAsync(user.IdUsuario)
                    .ConfigureAwait(false);


                var clientesUsuario = await _loginProvider.CardapioVirtualRepository.ConsultarClientesUsuarioAsync(user.IdUsuario)
                    .ConfigureAwait(false);

                List<UserClienteResponse> userClienteResponse = new List<UserClienteResponse>();
                UserClienteDeposito defaultWarehose = new UserClienteDeposito();

                var depositosUsuario = await _loginProvider.CardapioVirtualRepository.ConsultarDepositosUsuarioAsync(user.IdUsuario)
                    .ConfigureAwait(false);

                foreach (var clienteItem in clientesUsuario)
                {
                    UserClienteResponse userCliente = new UserClienteResponse();

                    userCliente.Cliente = await _loginProvider.CardapioVirtualRepository.ConsultarClientesAsync(clienteItem.IdCliente)
                        .ConfigureAwait (false);

                    userCliente.TipoAcesso = await _loginProvider.CardapioVirtualRepository.ConsultarTipoAcessoAsync(clienteItem.IdTipoAcesso)
                        .ConfigureAwait(false);

                    userCliente.TipoPessoa = await _loginProvider.CardapioVirtualRepository.ConsultarTipoPessoaAsync(userCliente.Cliente.IdTipoPessoa)
                        .ConfigureAwait(false);

                    userCliente.UserCliente = clienteItem;

                    userClienteResponse.Add(userCliente);  
                }

                var acessToken = await GenerateToken(user, userClienteResponse, userProfile, depositosUsuario).ConfigureAwait(false);

                return LoginResult.Succeed(user, acessToken, userClienteResponse, products);

            }catch(Exception e)
            {
                throw new Exception($"Critical error due to user login attempt - {e.Message}");
            }
        }

        public async Task<TokenAuthentication> RefreshUserToken(string key)
        {
            try
            {
                var user = await _loginProvider.UserRepository.GetUserByKeyAsync(key)
                    .ConfigureAwait(false);

                var accessKey = await _loginProvider.AccessKeyRepository.GetAccessKeyAsync(user.IdUsuario, Parameters.ProductId)
                    .ConfigureAwait(false);

                var products = await _loginProvider.ProductRepository.GetProductsAsync(user.IdUsuario)
                    .ConfigureAwait(false);
                
                var userProfile = await _loginProvider.UserRepository.GetUserProfilesAsync(user.IdUsuario)
                    .ConfigureAwait(false);

                var clientesUsuario = await _loginProvider.CardapioVirtualRepository.ConsultarClientesUsuarioAsync(user.IdUsuario)
                    .ConfigureAwait(false);

                List<UserClienteResponse> userClienteResponse = new List<UserClienteResponse>();
                UserClienteDeposito defaultWarehose = new UserClienteDeposito();

                var depositosUsuario = await _loginProvider.CardapioVirtualRepository.ConsultarDepositosUsuarioAsync(user.IdUsuario)
                    .ConfigureAwait(false);

                foreach (var clienteItem in clientesUsuario)
                {
                    UserClienteResponse userCliente = new UserClienteResponse();

                    userCliente.Cliente = await _loginProvider.CardapioVirtualRepository.ConsultarClientesAsync(clienteItem.IdCliente)
                        .ConfigureAwait(false);

                    userCliente.TipoAcesso = await _loginProvider.CardapioVirtualRepository.ConsultarTipoAcessoAsync(clienteItem.IdTipoAcesso)
                        .ConfigureAwait(false);

                    userCliente.TipoPessoa = await _loginProvider.CardapioVirtualRepository.ConsultarTipoPessoaAsync(userCliente.Cliente.IdTipoPessoa)
                        .ConfigureAwait(false);

                    userCliente.UserCliente = clienteItem;

                    userClienteResponse.Add(userCliente);
                }

                var acessToken = await GenerateToken(user, userClienteResponse, userProfile, depositosUsuario).ConfigureAwait(false);

                return acessToken;

            }
            catch (Exception e)
            {
                throw new Exception($"Critical error due to refresh user token attempt - {e.Message}");
            }
        }

        public void Initialize(LoginParameters parameters)
        {
            Parameters = parameters;
        }

        private async Task<TokenAuthentication> GenerateToken(User user, List<UserClienteResponse> userCliente, IEnumerable<Profile> profiles, IEnumerable<UserClienteDeposito> warehouses)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("fweonisovubqerobvjqdfbvçkqberivb");

            var defaultClient = userCliente.FirstOrDefault(x => x.UserCliente.IndAcessoPrincipal && x.Cliente.Ativo);

            var defaultWarehouse = new UserClienteDeposito();
            var clientsWarehouse = warehouses.Where(x => x.IdCliente == defaultClient.Cliente.IdCliente);

            if (clientsWarehouse != null)
            {
                defaultWarehouse = clientsWarehouse.FirstOrDefault(x => x.UltimoAcesso);

                if(defaultWarehouse == null)
                {
                    defaultWarehouse = clientsWarehouse.FirstOrDefault();
                }
            }

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim("user_id", user.IdUsuario.ToString()),
                    new Claim(ClaimTypes.Name, user.Name.ToString()),
                    new Claim(ClaimTypes.Email, user.Email.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            if (defaultWarehouse != null)
                tokenDescriptor.Subject.AddClaim(new Claim("default_warehouse_id", defaultWarehouse.IdDeposito.ToString()));

            if (defaultClient != null)
                tokenDescriptor.Subject.AddClaim(new Claim("agent_id", defaultClient.Cliente.IdCliente.ToString()));

            if (profiles != null && profiles.Any())
                tokenDescriptor.Subject.AddClaims(profiles.Select(_ => new Claim(ClaimTypes.Role, _.Sigla.ToString())).ToArray());

            var token = tokenHandler.CreateToken(tokenDescriptor);

            return new TokenAuthentication(tokenHandler.WriteToken(token), token.ValidFrom, token.ValidTo);
        }
    }
}
