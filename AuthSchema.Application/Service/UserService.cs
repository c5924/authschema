﻿using AuthSchema.Application.DTO.Request;
using AuthSchema.Application.Result;
using AuthSchema.Application.Service.Factories;
using AuthSchema.Application.Service.LoginStrategy;
using AuthSchema.Domain.Model;
using AuthSchema.Infrastructure.Data.UOW;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthSchema.Application.Service
{
    public class UserService : IUserService
    {
        private readonly ILogger<UserService> _logger;
        private readonly ILoginStrategyFactory _loginStrategyFactory;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        public UserService(ILogger<UserService> logger,
            ILoginStrategyFactory loginStrategyFactory,
            IUnitOfWorkFactory unitOfWorkFactory)
        {
            _logger = logger;
            _loginStrategyFactory = loginStrategyFactory;
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public async Task<LoginResult> UserLogin(LoginParameters parameters)
        {
            return await Login(_loginStrategyFactory.Create<UserLogin, User>(_ =>
            {
                _.ProductId = parameters.ProductId;
                _.Key = parameters.Key;
                _.Password = parameters.Password;
            })).ConfigureAwait(false);
        }

        private async Task<LoginResult> Login<TUser>(ILoginStrategy<TUser> loginStrategy)
            where TUser : User
        {
            var dataValidate = DataValidate(loginStrategy);
            if (dataValidate.IsInvalid)
                return dataValidate;

            return await loginStrategy.ExecuteLogin()
                .ConfigureAwait(false);
        }

        private static LoginResult DataValidate(ILoginStrategy loginStrategy)
        {
            if (string.IsNullOrWhiteSpace(loginStrategy.Parameters.Key))
                return LoginResult.InvalidData("Key should be Informed");

            if (string.IsNullOrWhiteSpace(loginStrategy.Parameters.Password))
                return LoginResult.InvalidData("Password should be Informed");

            return LoginResult.NoResult;
        }

        public async Task<TokenAuthentication> UserTokenRefresh(LoginParameters parameters)
        {
            return await TokenRefresh(_loginStrategyFactory.Create<UserLogin, User>(_ =>
            {
                _.ProductId = parameters.ProductId;
                _.Key = parameters.Key;
                _.Password = parameters.Password;
            })).ConfigureAwait(false);
        }

        private async Task<TokenAuthentication> TokenRefresh<TUser>(ILoginStrategy<TUser> loginStrategy)
            where TUser : User
        {
            var dataValidate = DataValidateTokenRefresh(loginStrategy);
            if (dataValidate.IsInvalid)
                return null;

            return await loginStrategy.RefreshUserToken(loginStrategy.Parameters.Key)
                .ConfigureAwait(false);
        }

        private static LoginResult DataValidateTokenRefresh(ILoginStrategy loginStrategy)
        {
            if (string.IsNullOrWhiteSpace(loginStrategy.Parameters.Key))
                return LoginResult.InvalidData("Key should be Informed");

            return LoginResult.NoResult;
        }

        public async Task<CadastroUsuarioResult> CadastraNovoUsuario(UserRequest request)
        {
            try
            {
                if (string.IsNullOrEmpty(request.Nome))
                    return CadastroUsuarioResult.DadosInvalidos("Nome é inválido");
                if (string.IsNullOrEmpty(request.Email))
                    return CadastroUsuarioResult.DadosInvalidos("Email é inválido");


                using (var uow = _unitOfWorkFactory.Create<UsuarioServiceUnifOfWork>())
                {
                    var verificaUsuario = await uow.UserRepository.GetUserByNameAndEmailAsync(request.Nome, request.Email)
                        .ConfigureAwait(false);

                    if (verificaUsuario != null)
                        return CadastroUsuarioResult.UsuariojaExiste(verificaUsuario);
                    //Persiste a criação
                    try
                    {
                        uow.Open();
                        User user = new User(request.Nome, request.Email);
                        user = await uow.UserRepository.AddUserAsync(user).ConfigureAwait(false);
                        
                        return CadastroUsuarioResult.Sucesso(user);
                    }
                    catch(Exception ex)
                    {
                        throw new Exception("Erro ao persistir usuário", ex);
                    }
                }

            }
            catch(Exception ex)
            {
                return CadastroUsuarioResult.ErroInterno(ex.Message);
            }
        }

        public async Task<UsuarioResult> ConsultarUsuario(int userId, string email)
        {
            try
            {
                if (userId <= 0 && string.IsNullOrEmpty(email))
                    return UsuarioResult.DadosInvalidos("deverá ser informado o código do usuário ou o email para consultar o usuário");

                if (userId < 0)
                    return UsuarioResult.DadosInvalidos("o código do usuário deve ser um número maior que 0");

                using (var uow = _unitOfWorkFactory.Create<UsuarioServiceUnifOfWork>())
                {
                    User retorno;
                    if(userId > 0)
                    {
                        retorno = await uow.UserRepository.GetUserByIdAsync(userId)
                            .ConfigureAwait(false);
                    }
                    else
                    {
                        retorno = await uow.UserRepository.GetUserByKeyAsync(email)
                            .ConfigureAwait(false);

                    }
                    if (retorno == null)
                        return UsuarioResult.UsuarioNaoEncontrado();

                    return UsuarioResult.Sucesso(retorno);

                }
            }
            catch (Exception ex) 
            {
                return UsuarioResult.ErroInterno(ex.Message);
            }
        }
    }
}
