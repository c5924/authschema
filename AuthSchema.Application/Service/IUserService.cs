﻿using AuthSchema.Application.DTO.Request;
using AuthSchema.Application.Result;
using AuthSchema.Application.Service.LoginStrategy;
using AuthSchema.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthSchema.Application.Service
{
    public interface IUserService
    {
        Task<LoginResult> UserLogin(LoginParameters parameters);
        Task<TokenAuthentication> UserTokenRefresh(LoginParameters parameters);

        Task<CadastroUsuarioResult> CadastraNovoUsuario(UserRequest request);
        Task<UsuarioResult> ConsultarUsuario(int userId, string email);
    }
}
