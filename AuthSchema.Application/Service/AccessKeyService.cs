﻿using AuthSchema.Application.Result;
using AuthSchema.Domain.Model;
using AuthSchema.Infrastructure.Config;
using AuthSchema.Infrastructure.Data.UOW;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthSchema.Application.Service
{
    public class AccessKeyService : IAccessKeyService
    {
        private readonly ILogger<UserService> _logger;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        public AccessKeyService(ILogger<UserService> logger,
            IUnitOfWorkFactory unitOfWorkFactory)
        {
            _logger = logger;
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public async Task<CadastroChaveAcessoResult> AlteraSenhaUsuarioAsync(string newPassword, int productId, int userId)
        {
            try
            {
                if (string.IsNullOrEmpty(newPassword))
                    return CadastroChaveAcessoResult.DadosInvalidos("nova senha é inválida");
                if (productId <= 0)
                    return CadastroChaveAcessoResult.DadosInvalidos("id do produto é inválido");
                if (userId <= 0)
                    return CadastroChaveAcessoResult.DadosInvalidos("id do usuário é inválido");


                using (var uowUser = _unitOfWorkFactory.Create<UsuarioServiceUnifOfWork>())
                {
                    var usuario = await uowUser.UserRepository.GetUserByIdAsync(userId)
                        .ConfigureAwait(false);

                    if (usuario == null)
                        return CadastroChaveAcessoResult.UsuarioNaoEncontrado();


                    using (var uow = _unitOfWorkFactory.Create<AccessKeyUnitOfWork>())
                    {
                        var produto = await uow.ProductRepository.GetProductAsync(productId)
                            .ConfigureAwait(false);

                        if (produto == null)
                            return CadastroChaveAcessoResult.ProdutoNaoEncontrado();

                        var consultaChaveAcesso = await uow.AccessKeyRepository.GetAccessKeyAsync(userId, productId)
                            .ConfigureAwait(false);

                        if (consultaChaveAcesso == null)
                            return CadastroChaveAcessoResult.ChaveDeAcessoNaoEncontradaParaoUsuario();

                        AccessKey chave = new AccessKey(consultaChaveAcesso.IdAccessKey, consultaChaveAcesso.AccessKeyName, SecurityHandler.EncryptPassword(newPassword), consultaChaveAcesso.Active, consultaChaveAcesso.Blocked, consultaChaveAcesso.CreationDate, consultaChaveAcesso.UpdatedDate, consultaChaveAcesso.ProductId, consultaChaveAcesso.UserID);

                        var atualizaChaveAcesso = await uow.AccessKeyRepository.UpdateAccessKeyAsync(chave)
                            .ConfigureAwait(false);

                        if (!atualizaChaveAcesso)
                            return CadastroChaveAcessoResult.ErroInterno("Erro ao cadastrrar a chave de acesso do usuario informado");

                        return CadastroChaveAcessoResult.Sucesso(userId);
                    }

                }

            }
            catch (Exception ex)
            {
                return CadastroChaveAcessoResult.ErroInterno(ex.Message);
            }
        }

        public async Task<CadastroChaveAcessoResult> CadastraChaveAcessoAsync(string password, int productId, int userId)
        {
            try
            {
                if (string.IsNullOrEmpty(password))
                    return CadastroChaveAcessoResult.DadosInvalidos("Senha é inválida");
                if (productId <= 0)
                    return CadastroChaveAcessoResult.DadosInvalidos("id do produto é inválido");
                if (userId <= 0)
                    return CadastroChaveAcessoResult.DadosInvalidos("id do usuário é inválido");


                using (var uowUser = _unitOfWorkFactory.Create<UsuarioServiceUnifOfWork>())
                {
                    var usuario = await uowUser.UserRepository.GetUserByIdAsync(userId)
                        .ConfigureAwait(false);

                    if (usuario == null)
                        return CadastroChaveAcessoResult.UsuarioNaoEncontrado();


                    using (var uow = _unitOfWorkFactory.Create<AccessKeyUnitOfWork>())
                    {
                        var produto = await uow.ProductRepository.GetProductAsync(productId)
                            .ConfigureAwait(false);

                        if (produto == null)
                            return CadastroChaveAcessoResult.ProdutoNaoEncontrado();

                        var consultaChaveAcesso = await uow.AccessKeyRepository.GetAccessKeyAsync(userId, productId)
                            .ConfigureAwait(false);

                        if (consultaChaveAcesso != null)
                            return CadastroChaveAcessoResult.ChaveDeAcessoJaExistente();

                        AccessKey chave = new AccessKey(usuario.Email,SecurityHandler.EncryptPassword(password), true, false, DateTime.Now, null, productId, userId);

                        var cadastraChaveAcesso = await uow.AccessKeyRepository.AddAccessKeyAsync(chave)
                            .ConfigureAwait(false);

                        if (cadastraChaveAcesso == null)
                            return CadastroChaveAcessoResult.ErroInterno("Erro ao cadastrrar a chave de acesso do usuario informado");

                        return CadastroChaveAcessoResult.Sucesso(userId);
                    }

                }

            }
            catch (Exception ex)
            {
                return CadastroChaveAcessoResult.ErroInterno(ex.Message);
            }
        }

        public async Task<CadastroChaveAcessoResult> AlteraChaveAcessoAsync(int productId, int userId, bool ativo, bool bloqueado)
        {
            try
            {
                if (productId <= 0)
                    return CadastroChaveAcessoResult.DadosInvalidos("id do produto é inválido");
                if (userId <= 0)
                    return CadastroChaveAcessoResult.DadosInvalidos("id do usuário é inválido");


                using (var uowUser = _unitOfWorkFactory.Create<UsuarioServiceUnifOfWork>())
                {
                    var usuario = await uowUser.UserRepository.GetUserByIdAsync(userId)
                        .ConfigureAwait(false);

                    if (usuario == null)
                        return CadastroChaveAcessoResult.UsuarioNaoEncontrado();


                    using (var uow = _unitOfWorkFactory.Create<AccessKeyUnitOfWork>())
                    {
                        var produto = await uow.ProductRepository.GetProductAsync(productId)
                            .ConfigureAwait(false);

                        if (produto == null)
                            return CadastroChaveAcessoResult.ProdutoNaoEncontrado();

                        var consultaChaveAcesso = await uow.AccessKeyRepository.GetAccessKeyAsync(userId, productId, false)
                            .ConfigureAwait(false);

                        if (consultaChaveAcesso == null)
                            return CadastroChaveAcessoResult.ChaveDeAcessoNaoEncontradaParaoUsuario();

                        AccessKey chave = new AccessKey(consultaChaveAcesso.IdAccessKey,consultaChaveAcesso.AccessKeyName, consultaChaveAcesso.AccessKeyAttr, ativo, bloqueado, consultaChaveAcesso.CreationDate,
                            consultaChaveAcesso.UpdatedDate, consultaChaveAcesso.ProductId, consultaChaveAcesso.UserID);

                        var cadastraChaveAcesso = await uow.AccessKeyRepository.UpdateAccessKeyAsync(chave)
                            .ConfigureAwait(false);

                        if (!cadastraChaveAcesso)
                            return CadastroChaveAcessoResult.ErroInterno("Erro ao cadastrrar a chave de acesso do usuario informado");

                        return CadastroChaveAcessoResult.Sucesso(userId);
                    }

                }

            }
            catch (Exception ex)
            {
                return CadastroChaveAcessoResult.ErroInterno(ex.Message);
            }
        }
    }
}
