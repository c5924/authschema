﻿using AuthSchema.Application.Service.LoginStrategy;
using AuthSchema.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthSchema.Application.Service.Factories
{
    public class LoginStrategyFactory : ILoginStrategyFactory
    {
        private readonly Func<Type, ILoginStrategy> _loginStrategyFactory;

        public LoginStrategyFactory(Func<Type, ILoginStrategy> loginStrategyFactory)
        {
            _loginStrategyFactory = loginStrategyFactory;
        }

        public ILoginStrategy<TUser> Create<T, TUser>(Action<LoginParameters> optionsAction)
            where T : ILoginStrategy<TUser>
            where TUser : User
        {
            var loginStrategy = _loginStrategyFactory(typeof(T));

            if (loginStrategy == null)
            {
                throw new InvalidOperationException($"Login strategy of type {typeof(T).Name} for user type {typeof(TUser).Name} not found.");
            }

            var parameters = new LoginParameters();
            optionsAction(parameters);
            loginStrategy.Initialize(parameters);
            return (ILoginStrategy<TUser>)loginStrategy;
        }
    }


}
