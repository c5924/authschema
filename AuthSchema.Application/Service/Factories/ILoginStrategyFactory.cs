﻿using AuthSchema.Application.Service.LoginStrategy;
using AuthSchema.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthSchema.Application.Service.Factories
{
    public interface ILoginStrategyFactory
    {
        ILoginStrategy<TUser> Create<T, TUser>(Action<LoginParameters> optionsAction)
            where T : ILoginStrategy<TUser>
            where TUser : User;
    }
}
