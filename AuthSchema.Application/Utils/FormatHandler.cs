﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AuthSchema.Application.Utils
{
    public class FormatHandler
    {
        public static string FormatDocument(string document, int documentType)
        {
            // Remove todos os caracteres não numéricos do documento
            string cleanDocument = Regex.Replace(document, "[^0-9]", "");

            // Verifica o tipo de documento e aplica a formatação correspondente
            //Pessoa Fisica
            if (documentType == 1)
            {
                // Formatação do CPF (###.###.###-##)
                return Regex.Replace(cleanDocument, @"(\d{3})(\d{3})(\d{3})(\d{2})", "$1.$2.$3-$4");
            }
            //Pessoa Juridica
            else if (documentType == 2)
            {
                // Formatação do CNPJ (##.###.###/####-##)
                return Regex.Replace(cleanDocument, @"(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})", "$1.$2.$3/$4-$5");
            }
            else
            {
                // Caso o tipo de documento seja inválido, retorna o documento sem formatação
                return document;
            }
        }

        public static string FormatEmail(string email)
        {
            int atIndex = email.IndexOf('@');
            int dotIndex = email.LastIndexOf('.');

            if (atIndex >= 0 && dotIndex > atIndex)
            {
                string username = email.Substring(0, atIndex);
                string domain = email.Substring(atIndex, dotIndex - atIndex);
                string extension = email.Substring(dotIndex);

                // Substitui todos os caracteres no username, exceto as duas primeiras letras
                string formattedUsername = $"{username.Substring(0, 2)}{new string('*', username.Length - 2)}";

                return $"{formattedUsername}{domain}{extension}";
            }

            return email; // Retorna o email sem formatação caso não siga o formato esperado
        }
    }
}
