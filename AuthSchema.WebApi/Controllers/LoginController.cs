﻿using AuthSchema.Application.DTO.Request.Identificadores;
using AuthSchema.Application.Service;
using AuthSchema.Application.Service.LoginStrategy;
using AuthSchema.Application.Utils;
using AuthSchema.WebApi.Model.Request;
using AuthSchema.WebApi.Model.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace AuthSchema.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/user/login")]
    [ApiVersion("1.0")]
    public class LoginController : Controller
    {
        private readonly ILogger _logger;
        private readonly IUserService _userService;
        public LoginController(ILogger<UserController> logger,
            IUserService userService)
        {
            _logger = logger;
            _userService = userService;
        }

        [HttpPost]
        [AllowAnonymous]
        [SwaggerResponse(StatusCodes.Status200OK, "UserLogin", typeof(UserLoginResponse))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "BadRequestResponse", typeof(BadRequestErrorResponse))]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Unauthorized")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Forbidden")]
        public async Task<IActionResult> UserLogin([FromBody] LoginRequest model)
        {
            string prefix = "auth.usr.log";

            try
            {
                _logger.LogInformation($"Tentativa de Login de Usuário: {model.Key} - Data: {DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")}");

                LoginParameters parameters = new LoginParameters()
                {
                    Key = model.Key,
                    Password = model.Password,
                    ProductId = model.Controle.Produto._Id
                };

                var result = await _userService.UserLogin(parameters)
                    .ConfigureAwait(false);

                if (result.Status == Application.Result.LoginStatus.InvalidData)
                {
                    return Unauthorized();
                }
                else if (result.Status == Application.Result.LoginStatus.Error)
                {
                    return BadRequest(
                    new BadRequestErrorResponse()
                    {
                        Error = new ErrorResponse(result.ErrorMessage),
                        Controle = new ControleResponse()
                        {
                            prefix = $"{prefix}.503",
                            message = "Usuário ou senha inválidos, verifique os dados informados e tente novamente"
                        }
                    });
                }
                else if (result.Status == Application.Result.LoginStatus.BlockedUser)
                {
                    return BadRequest(
                    new UsuarioBloqueadoInativoResponse()
                    {
                        Usuario = new IdentificadorUsuarioRequest
                        {
                            _Id = result.User.IdUsuario
                        },
                        Controle = new ControleResponse()
                        {
                             prefix = $"{prefix}.503",
                             message = result.ErrorMessage
                        }
                    });
                }

                return Ok(
                    new LoginResponse
                    {
                        Controle = new ControleResponse
                        {
                            prefix = $"{prefix}.001",
                            message = "Login realizado com sucesso!"
                        },
                        User = new UserLoginResponse()
                        {
                            _Id = result.User.IdUsuario,
                            Email = result.User.Email,
                            MaskedEmail = FormatHandler.FormatEmail(result.User.Email),
                            Name = result.User.Name,
                            Agents = result.UserClientes?.Select(_ => new ClienteUsuarioResponse
                            {
                                Agent = new ClienteResponse
                                {
                                    _Id = _.Cliente.IdCliente,
                                    Name = _.Cliente.Nome,
                                    PersonCategory = new TipoPessoaResponse
                                    {
                                        _Id = _.TipoPessoa.IdTipoPessoa,
                                        Description = _.TipoPessoa.Descricao
                                    },
                                    Document = new DocumentoResponse
                                    {
                                        _Id = _.Cliente.IdCliente,
                                        Document = _.Cliente.Inscricao,
                                        MaskedDocument = FormatHandler.FormatDocument(_.Cliente.Inscricao, _.TipoPessoa.IdTipoPessoa)
                                    },
                                    Active = _.Cliente.Ativo,
                                    Logo = _.Cliente.Logo,
                                    Access = new TipoAcessoResponse
                                    {
                                        _Id = _.TipoAcesso.IdTipoAcesso,
                                        Description = _.TipoAcesso.Descricao,
                                        FirstAccess = _.UserCliente.IndPrimeiroAcesso,
                                        MainAccess = _.UserCliente.IndAcessoPrincipal,
                                        InactivationAccessDate = _.UserCliente.DataInativacaoAcesso,
                                        LastAccess = _.UserCliente.DataUltimoAcesso,
                                        SignUpDate = _.UserCliente.DataCadastro
                                    }
                                }
                            }),
                            Products = result.Products?.Select(_ => new UsuarioProdutoResponse
                            {
                                Product = new ProdutoResponse
                                {
                                    _Id = _.IdProduto,
                                    Description = _.Descricao,
                                    Name = _.Nome
                                }
                            }),
                            AccessToken = new TokenAuthenticationResponse()
                            {
                                Token = result.AccessToken.Token,
                                CreationDate = result.AccessToken.CreationDate,
                                ExpirationDate = result.AccessToken.ExpirationDate
                            }
                        }
                    });
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return BadRequest(
                    new BadRequestErrorResponse()
                    {
                        Error = new ErrorResponse($"{prefix}.400 - {e.Message}"),
                        Controle = new ControleResponse()
                        {
                            prefix = $"{prefix}.503",
                            message = "Erro crítico ao tentar realizar o login do usuário"
                        }
                    }
                );
            }
        }
    }
}