﻿using AuthSchema.Application.DTO.Request;
using AuthSchema.Application.DTO.Response;
using AuthSchema.Application.DTO.Response.Identificadores;
using AuthSchema.Application.Result;
using AuthSchema.Application.Service;
using AuthSchema.Application.Service.LoginStrategy;
using AuthSchema.WebApi.Model.Request;
using AuthSchema.WebApi.Model.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthSchema.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/user")]
    [ApiVersion("1.0")]
    public class UserController : Controller
    {
        private readonly ILogger _logger;
        private readonly IUserService _userService;
        private readonly IAccessKeyService _accessKeyService;
        public UserController(ILogger<UserController> logger,
            IUserService userService,
            IAccessKeyService accessKeyService)
        {
            _logger = logger;
            _userService = userService;
            _accessKeyService = accessKeyService;
        }

        [HttpGet]
        [HttpGet("{userId:int}")]
        [Authorize]
        [SwaggerResponse(StatusCodes.Status200OK, "ConsultarUsuario", typeof(UserResponse))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "BadRequestResponse", typeof(BadRequestErrorResponse))]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Unauthorized")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Forbidden")]
        public async Task<IActionResult> ConsultaUsuario([FromRoute] int userId, [FromQuery] string email)
        {
            string prefix = "aut.usr.cusu";
            try
            {
                #region Validação de entrada de dados
                if (userId <= 0 && string.IsNullOrEmpty(email))
                    return BadRequest(new BadRequestErrorResponse
                    {
                        Error = new ErrorResponse("Deverá ser informado o Código ou Email do usuário para consultar"),
                        Controle = new ControleResponse
                        {
                            message = "Dados inválidos ou incompletos",
                            prefix = prefix
                        }
                    });

                if (userId < 0)
                    return BadRequest(new BadRequestErrorResponse
                    {
                        Error = new ErrorResponse("o código do usuário deve ser um número maior que 0"),
                        Controle = new ControleResponse
                        {
                            message = "Dados inválidos ou incompletos",
                            prefix = prefix
                        }
                    });
                #endregion


                var result = await _userService.ConsultarUsuario(userId, email)
                    .ConfigureAwait(false);

                if (result.Status == UsuarioStatus.Sucesso)
                    return Ok(new UserResponse()
                    {
                        Usuario = new UsuarioResponse
                        {
                            _Id = result.User.IdUsuario,
                            Email = result.User.Email,
                            Name = result.User.Name
                        },
                        Controle = new ControleResponse()
                        {
                            message = "usuário disponibilizado com sucesso!",
                            prefix = $"{prefix}.001"
                        }
                    });

                if(result.Status == UsuarioStatus.UsuarioNaoEncontrado)
                    return BadRequest(new BadRequestErrorResponse
                    {
                        Error = new ErrorResponse(result.ErrorMessage),
                        Controle = new ControleResponse
                        {
                            message = "Usuário não existente ou não encontrado",
                            prefix = $"{prefix}.404",
                        }
                    });
                if (result.Status == UsuarioStatus.DadosInvalidos)
                    return BadRequest(new BadRequestErrorResponse
                    {
                        Error = new ErrorResponse(result.ErrorMessage),
                        Controle = new ControleResponse
                        {
                            message = "Dados inválidos ou Imcompletos",
                            prefix = $"{prefix}.503",
                        }
                    });
                return BadRequest(new BadRequestErrorResponse
                {
                    Error = new ErrorResponse(result.ErrorMessage),
                    Controle = new ControleResponse
                    {
                        message = "Erro ao consultar o usuário",
                        prefix = $"{prefix}.502",
                    }
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new BadRequestErrorResponse
                {
                    Error = new ErrorResponse(ex.Message),
                    Controle = new ControleResponse
                    {
                        message = "Erro ao consultar o usuário",
                        prefix = $"{prefix}.501",
                    }
                });
            }
        }

        [Authorize]
        [HttpPost]
        [SwaggerResponse(StatusCodes.Status200OK, "CadastraNovoUsuario", typeof(NovoUsuarioResponse))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "BadRequestResponse", typeof(BadRequestErrorResponse))]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Unauthorized")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Forbidden")]
        public async Task<IActionResult> CadastraNovoUsuario([FromBody] NovoUsuarioRequest request)
        {
            string prefix = "aut.usr.cnu";
            try
            {
                #region Validar Dados Request
                if(request == null)
                    return BadRequest(new BadRequestErrorResponse
                    {
                        Error = new ErrorResponse("modelo de requisição inválido ou nao informado"),
                        Controle = new ControleResponse
                        {
                            message = "Dados inválidos ou incompletos",
                            prefix = prefix
                        }
                    });

                if (string.IsNullOrEmpty(request.NomeUsuario))
                    return BadRequest(new BadRequestErrorResponse
                    {
                        Error = new ErrorResponse("Nome do usuário inválido ou nao informado"),
                        Controle = new ControleResponse
                        {
                            message = "Dados inválidos ou incompletos",
                            prefix = prefix
                        }
                    });
                if (string.IsNullOrEmpty(request.Email))
                    return BadRequest(new BadRequestErrorResponse
                    {
                        Error = new ErrorResponse("Email do usuário inválido ou nao informado"),
                        Controle = new ControleResponse
                        {
                            message = "Dados inválidos ou incompletos",
                            prefix = prefix
                        }
                    });
                #endregion

                var usuario = new UserRequest
                {
                    Nome = request.NomeUsuario,
                    Email = request.Email
                };

                var result = await _userService.CadastraNovoUsuario(usuario)
                    .ConfigureAwait(false);

                if (result.Status == CadastroUsuarioStatus.Sucesso)
                    return Ok(new NovoUsuarioResponse
                    {
                        Controle = new ControleResponse
                        {
                            prefix = $"{prefix}.001",
                            message = "usuário cadastrado com sucesso!"
                        },
                        Usuario = new IdentificadorUsuarioResponse
                        {
                            _Id = result.Usuario.IdUsuario
                        }
                    });

                if(result.Status == CadastroUsuarioStatus.DadosInvalidos)
                    return BadRequest(new BadRequestErrorResponse
                    {
                        Error = new ErrorResponse(result.ErrorMessage),
                        Controle = new ControleResponse
                        {
                            message = "Dados inválidos ou incompletos",
                            prefix = $"{prefix}.503",
                        }
                    });

                if (result.Status == CadastroUsuarioStatus.UsuarioJaExiste)
                    return BadRequest(new BadRequestErrorResponse
                    {
                        Error = new ErrorResponse(result.ErrorMessage),
                        Controle = new ControleResponse
                        {
                            message = "usuário já existente",
                            prefix = $"{prefix}.502",
                        }
                    });

                return BadRequest(new BadRequestErrorResponse
                {
                    Error = new ErrorResponse(result.ErrorMessage),
                    Controle = new ControleResponse
                    {
                        message = "erro ao processar a solicitação",
                        prefix = $"{prefix}.504",
                    }
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new BadRequestErrorResponse
                {
                    Error = new ErrorResponse(ex.Message),
                    Controle = new ControleResponse
                    {
                        message = "Erro ao tentar cadastrar o usuário",
                        prefix = $"{prefix}.501",
                    }
                });
            }
        }

        [Authorize]
        [HttpPost("acesso")]
        [SwaggerResponse(StatusCodes.Status200OK, "CadastraChaveAcessoAsync", typeof(ChaveAcessoResponse))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "BadRequestResponse", typeof(BadRequestErrorResponse))]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Unauthorized")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Forbidden")]
        public async Task<IActionResult> CadastraChaveAcessoUsuario([FromBody] CadastraChaveAcessoUsuarioRequest request)
        {
            string prefix = "aut.usr.ccau";
            try
            {
                #region Validar Dados Request
                if (request == null)
                    return BadRequest(new BadRequestErrorResponse
                    {
                        Error = new ErrorResponse("modelo de requisição inválido ou nao informado"),
                        Controle = new ControleResponse
                        {
                            message = "Dados inválidos ou incompletos",
                            prefix = prefix
                        }
                    });

                if (string.IsNullOrEmpty(request.Password))
                    return BadRequest(new BadRequestErrorResponse
                    {
                        Error = new ErrorResponse("Password inválido ou nao informado"),
                        Controle = new ControleResponse
                        {
                            message = "Dados inválidos ou incompletos",
                            prefix = prefix
                        }
                    });
                if (request.Produto == null || request.Produto._Id <= 0)
                    return BadRequest(new BadRequestErrorResponse
                    {
                        Error = new ErrorResponse("código do produto inválido ou não informado"),
                        Controle = new ControleResponse
                        {
                            message = "Dados inválidos ou incompletos",
                            prefix = prefix
                        }
                    });
                if (request.Usuario == null || request.Usuario._Id <= 0)
                    return BadRequest(new BadRequestErrorResponse
                    {
                        Error = new ErrorResponse("código do usuário inválido ou não informado"),
                        Controle = new ControleResponse
                        {
                            message = "Dados inválidos ou incompletos",
                            prefix = prefix
                        }
                    });
                #endregion

                var result = await _accessKeyService.CadastraChaveAcessoAsync(request.Password, request.Produto._Id, request.Usuario._Id)
                    .ConfigureAwait(false);

                if (result.Status == CadastroChaveAcessoStatus.Sucesso)
                    return Ok(new ChaveAcessoResponse
                    {
                        Controle = new ControleResponse
                        {
                            prefix = $"{prefix}.001",
                            message = "Chave de acesso cadastrada com sucesso!",
                        },
                        Usuario = new IdentificadorUsuarioResponse
                        {
                            _Id = result.Usuario._Id,
                        }
                    });

                    if (result.Status == CadastroChaveAcessoStatus.DadosInvalidos)
                    return BadRequest(new BadRequestErrorResponse
                    {
                        Error = new ErrorResponse(result.ErrorMessage),
                        Controle = new ControleResponse
                        {
                            message = "Dados inválidos ou incompletos",
                            prefix = $"{prefix}.503",
                        }
                    });

                if (result.Status == CadastroChaveAcessoStatus.UsuarioNaoEncontrado)
                    return BadRequest(new BadRequestErrorResponse
                    {
                        Error = new ErrorResponse(result.ErrorMessage),
                        Controle = new ControleResponse
                        {
                            message = "usuário não encontrado",
                            prefix = $"{prefix}.502",
                        }
                    });

                if (result.Status == CadastroChaveAcessoStatus.ProdutoNaoEncontrado)
                    return BadRequest(new BadRequestErrorResponse
                    {
                        Error = new ErrorResponse(result.ErrorMessage),
                        Controle = new ControleResponse
                        {
                            message = "produto não encontrado",
                            prefix = $"{prefix}.502",
                        }
                    });

                if (result.Status == CadastroChaveAcessoStatus.ChaveDeAcessoJaExistente)
                    return BadRequest(new BadRequestErrorResponse
                    {
                        Error = new ErrorResponse(result.ErrorMessage),
                        Controle = new ControleResponse
                        {
                            message = "chave de acesso já existe",
                            prefix = $"{prefix}.502",
                        }
                    });

                return BadRequest(new BadRequestErrorResponse
                {
                    Error = new ErrorResponse(result.ErrorMessage),
                    Controle = new ControleResponse
                    {
                        message = "erro ao processar a solicitação",
                        prefix = $"{prefix}.504",
                    }
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new BadRequestErrorResponse
                {
                    Error = new ErrorResponse(ex.Message),
                    Controle = new ControleResponse
                    {
                        message = "Erro ao tentar cadastrar a chave de accesso",
                        prefix = $"{prefix}.501",
                    }
                });
            }
        }

        [Authorize]
        [HttpPut("acesso")]
        [SwaggerResponse(StatusCodes.Status200OK, "AlteraChaveAcessoAsync", typeof(ChaveAcessoResponse))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "BadRequestResponse", typeof(BadRequestErrorResponse))]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Unauthorized")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Forbidden")]
        public async Task<IActionResult> EditaChaveAcessoUsuario([FromBody] EditaChaveAcessoUsuarioRequest request)
        {
            string prefix = "aut.usr.ecau";
            try
            {
                #region Validar Dados Request
                if (request == null)
                    return BadRequest(new BadRequestErrorResponse
                    {
                        Error = new ErrorResponse("modelo de requisição inválido ou nao informado"),
                        Controle = new ControleResponse
                        {
                            message = "Dados inválidos ou incompletos",
                            prefix = prefix
                        }
                    });

                if (request.Produto == null || request.Produto._Id <= 0)
                    return BadRequest(new BadRequestErrorResponse
                    {
                        Error = new ErrorResponse("código do produto inválido ou não informado"),
                        Controle = new ControleResponse
                        {
                            message = "Dados inválidos ou incompletos",
                            prefix = prefix
                        }
                    });
                if (request.Usuario == null || request.Usuario._Id <= 0)
                    return BadRequest(new BadRequestErrorResponse
                    {
                        Error = new ErrorResponse("código do usuário inválido ou não informado"),
                        Controle = new ControleResponse
                        {
                            message = "Dados inválidos ou incompletos",
                            prefix = prefix
                        }
                    });
                #endregion

                var result = await _accessKeyService.AlteraChaveAcessoAsync( request.Produto._Id, request.Usuario._Id, request.Ativo, request.Bloqueado)
                    .ConfigureAwait(false);

                if (result.Status == CadastroChaveAcessoStatus.Sucesso)
                    return Ok(new ChaveAcessoResponse
                    {
                        Controle = new ControleResponse
                        {
                            prefix = $"{prefix}.001",
                            message = "Chave de acesso editada com sucesso!",
                        },
                        Usuario = new IdentificadorUsuarioResponse
                        {
                            _Id = result.Usuario._Id,
                        }
                    });

                if (result.Status == CadastroChaveAcessoStatus.DadosInvalidos)
                    return BadRequest(new BadRequestErrorResponse
                    {
                        Error = new ErrorResponse(result.ErrorMessage),
                        Controle = new ControleResponse
                        {
                            message = "Dados inválidos ou incompletos",
                            prefix = $"{prefix}.503",
                        }
                    });

                if (result.Status == CadastroChaveAcessoStatus.UsuarioNaoEncontrado)
                    return BadRequest(new BadRequestErrorResponse
                    {
                        Error = new ErrorResponse(result.ErrorMessage),
                        Controle = new ControleResponse
                        {
                            message = "usuário não encontrado",
                            prefix = $"{prefix}.502",
                        }
                    });

                if (result.Status == CadastroChaveAcessoStatus.ProdutoNaoEncontrado)
                    return BadRequest(new BadRequestErrorResponse
                    {
                        Error = new ErrorResponse(result.ErrorMessage),
                        Controle = new ControleResponse
                        {
                            message = "produto não encontrado",
                            prefix = $"{prefix}.502",
                        }
                    });

                if (result.Status == CadastroChaveAcessoStatus.ChaveDeAcessoJaExistente)
                    return BadRequest(new BadRequestErrorResponse
                    {
                        Error = new ErrorResponse(result.ErrorMessage),
                        Controle = new ControleResponse
                        {
                            message = "chave de acesso já existe",
                            prefix = $"{prefix}.502",
                        }
                    });

                return BadRequest(new BadRequestErrorResponse
                {
                    Error = new ErrorResponse(result.ErrorMessage),
                    Controle = new ControleResponse
                    {
                        message = "erro ao processar a solicitação",
                        prefix = $"{prefix}.504",
                    }
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new BadRequestErrorResponse
                {
                    Error = new ErrorResponse(ex.Message),
                    Controle = new ControleResponse
                    {
                        message = "Erro ao tentar editar a chave de accesso",
                        prefix = $"{prefix}.501",
                    }
                });
            }
        }

        [Authorize]
        [HttpPost("senha")]
        [SwaggerResponse(StatusCodes.Status200OK, "AlteraSenhaUsuarioAsync", typeof(ChaveAcessoResponse))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "BadRequestResponse", typeof(BadRequestErrorResponse))]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Unauthorized")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Forbidden")]
        public async Task<IActionResult> AlteraSenhaUsuario([FromBody] AlteraSenhaUsuarioRequest request)
        {
            string prefix = "aut.usr.sen";
            try
            {
                #region Validar Dados Request
                if (request == null)
                    return BadRequest(new BadRequestErrorResponse
                    {
                        Error = new ErrorResponse("modelo de requisição inválido ou nao informado"),
                        Controle = new ControleResponse
                        {
                            message = "Dados inválidos ou incompletos",
                            prefix = prefix
                        }
                    });

                if (string.IsNullOrEmpty(request.NovaSenha))
                    return BadRequest(new BadRequestErrorResponse
                    {
                        Error = new ErrorResponse("a nova senha é inválido ou nao informado"),
                        Controle = new ControleResponse
                        {
                            message = "Dados inválidos ou incompletos",
                            prefix = prefix
                        }
                    });
                if (request.Produto == null || request.Produto._Id <= 0)
                    return BadRequest(new BadRequestErrorResponse
                    {
                        Error = new ErrorResponse("código do produto inválido ou não informado"),
                        Controle = new ControleResponse
                        {
                            message = "Dados inválidos ou incompletos",
                            prefix = prefix
                        }
                    });
                if (request.Usuario == null || request.Usuario._Id <= 0)
                    return BadRequest(new BadRequestErrorResponse
                    {
                        Error = new ErrorResponse("código do usuário inválido ou não informado"),
                        Controle = new ControleResponse
                        {
                            message = "Dados inválidos ou incompletos",
                            prefix = prefix
                        }
                    });
                #endregion

                var result = await _accessKeyService.AlteraSenhaUsuarioAsync(request.NovaSenha, request.Produto._Id, request.Usuario._Id)
                    .ConfigureAwait(false);

                if (result.Status == CadastroChaveAcessoStatus.Sucesso)
                    return Ok(new ChaveAcessoResponse
                    {
                        Controle = new ControleResponse
                        {
                            prefix = $"{prefix}.001",
                            message = "senha alterada com sucesso!",
                        },
                        Usuario = new IdentificadorUsuarioResponse
                        {
                            _Id = result.Usuario._Id,
                        }
                    });

                if (result.Status == CadastroChaveAcessoStatus.DadosInvalidos)
                    return BadRequest(new BadRequestErrorResponse
                    {
                        Error = new ErrorResponse(result.ErrorMessage),
                        Controle = new ControleResponse
                        {
                            message = "Dados inválidos ou incompletos",
                            prefix = $"{prefix}.503",
                        }
                    });

                if (result.Status == CadastroChaveAcessoStatus.UsuarioNaoEncontrado)
                    return BadRequest(new BadRequestErrorResponse
                    {
                        Error = new ErrorResponse(result.ErrorMessage),
                        Controle = new ControleResponse
                        {
                            message = "usuário não encontrado",
                            prefix = $"{prefix}.502",
                        }
                    });

                if (result.Status == CadastroChaveAcessoStatus.ProdutoNaoEncontrado)
                    return BadRequest(new BadRequestErrorResponse
                    {
                        Error = new ErrorResponse(result.ErrorMessage),
                        Controle = new ControleResponse
                        {
                            message = "produto não encontrado",
                            prefix = $"{prefix}.502",
                        }
                    });

                if (result.Status == CadastroChaveAcessoStatus.ChaveDeAcessoNaoEncontradaParaoUsuario)
                    return BadRequest(new BadRequestErrorResponse
                    {
                        Error = new ErrorResponse(result.ErrorMessage),
                        Controle = new ControleResponse
                        {
                            message = "chave de acesso não encontrada para o usuário",
                            prefix = $"{prefix}.502",
                        }
                    });

                return BadRequest(new BadRequestErrorResponse
                {
                    Error = new ErrorResponse(result.ErrorMessage),
                    Controle = new ControleResponse
                    {
                        message = "erro ao processar a solicitação",
                        prefix = $"{prefix}.504",
                    }
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new BadRequestErrorResponse
                {
                    Error = new ErrorResponse(ex.Message),
                    Controle = new ControleResponse
                    {
                        message = "Erro ao tentar alterar a chave de accesso do usuário",
                        prefix = $"{prefix}.501",
                    }
                });
            }
        }


        [HttpPost("refresh/token")]
        [Authorize]
        [SwaggerResponse(StatusCodes.Status200OK, "UserLogin", typeof(UserLoginResponse))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "BadRequestResponse", typeof(BadRequestErrorResponse))]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Unauthorized")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Forbidden")]
        public async Task<IActionResult> RefreshUserToken([FromBody] RefreshTokenRequest model)
        {
            string prefix = "auth.usr.log";

            try
            {
                LoginParameters parameters = new LoginParameters()
                {
                    Key = model.Key
                };


                _logger.LogInformation($"Tentativa de Login de Usuário: {model.Key} - Data: {DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")}");

                var result = await _userService.UserTokenRefresh(parameters)
                    .ConfigureAwait(false);

                if (result == null)
                {
                    return BadRequest(
                        new BadRequestErrorResponse()
                        {
                            Error = new ErrorResponse("Ocorreu um erro ao refrescar o token do usuário"),
                            Controle = new ControleResponse()
                            {
                                prefix = $"{prefix}.503",
                                message = "Ocorreu um erro ao refrescar o token do usuário"
                            }
                        });
                }

                return Ok(new RefreshTokenResponse
                {
                    AccessToken = new TokenAuthenticationResponse()
                    {
                        Token = result.Token,
                        CreationDate = result.CreationDate,
                        ExpirationDate = result.ExpirationDate
                    },
                    Controle = new ControleResponse()
                    {
                        prefix = $"{prefix}.001",
                        message = "sucesso ao refrescar o token do usuário"
                    }
                });

            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return BadRequest(
                    new BadRequestErrorResponse()
                    {
                        Error = new ErrorResponse($"{prefix}.400 - {e.Message}"),
                        Controle = new ControleResponse()
                        {
                            prefix = $"{prefix}.503",
                            message = "Erro crítico ao tentar realizar o login do usuário"
                        }
                    }
                );
            }
        }
    }
}
