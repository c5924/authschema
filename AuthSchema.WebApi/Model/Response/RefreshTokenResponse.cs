﻿namespace AuthSchema.WebApi.Model.Response
{
    public class RefreshTokenResponse
    {
        public TokenAuthenticationResponse AccessToken { get; set; }
        public ControleResponse Controle { get; set; }
    }
}
