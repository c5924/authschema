﻿using AuthSchema.Application.DTO.Response.Identificadores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthSchema.WebApi.Model.Response
{
    public class NovoUsuarioResponse
    {
        public IdentificadorUsuarioResponse Usuario { get; set; }
        public ControleResponse Controle { get; set; }
    }
}
