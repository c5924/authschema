﻿using System;

namespace AuthSchema.WebApi.Model.Response
{
    public class TokenAuthenticationResponse
    {
        public string Token { get; set; }
        public DateTime ExpirationDate { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
