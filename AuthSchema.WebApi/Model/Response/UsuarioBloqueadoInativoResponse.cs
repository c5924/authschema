﻿using AuthSchema.Application.DTO.Request.Identificadores;

namespace AuthSchema.WebApi.Model.Response
{
    public class UsuarioBloqueadoInativoResponse
    {
        public IdentificadorUsuarioRequest Usuario { get; set; }
        public ControleResponse Controle { get; set; }
    }
}
