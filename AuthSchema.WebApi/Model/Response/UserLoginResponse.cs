﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthSchema.WebApi.Model.Response
{
    public class UserLoginResponse
    {
        public int _Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string MaskedEmail { get; set; }
        public IEnumerable<ClienteUsuarioResponse> Agents { get; set; }
        public IEnumerable<UsuarioProdutoResponse> Products { get; set; }
        public TokenAuthenticationResponse AccessToken { get; set; }
    }
}
