﻿namespace AuthSchema.WebApi.Model.Response
{
    public class UsuarioProdutoResponse
    {
        public ProdutoResponse Product { get; set; }
    }

    public class ProdutoResponse
    {
        public int _Id { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
    }
}
