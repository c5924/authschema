﻿namespace AuthSchema.WebApi.Model.Response
{
    public class LoginResponse
    {
        public UserLoginResponse User { get; set; }
        public ControleResponse Controle { get; set; }
    }
}
