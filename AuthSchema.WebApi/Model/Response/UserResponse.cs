﻿using AuthSchema.Application.DTO.Response;

namespace AuthSchema.WebApi.Model.Response
{
    public class UserResponse
    {
        public UsuarioResponse Usuario { get; set; }
        public ControleResponse Controle { get; set; }
    }
}
