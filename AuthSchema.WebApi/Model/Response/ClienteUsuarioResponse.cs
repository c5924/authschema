﻿using System;

namespace AuthSchema.WebApi.Model.Response
{
    public class ClienteUsuarioResponse
    {
        public ClienteResponse Agent { get; set; }
    }

    public class ClienteResponse
    {
        public int _Id { get; set; }
        public string Name{ get; set; }
        public bool Active { get; set; }
        public string Logo { get; set; }
        public TipoPessoaResponse PersonCategory { get; set; }
        public DocumentoResponse Document { get; set; }
        public TipoAcessoResponse Access { get; set; }
    }

    public class TipoPessoaResponse
    {
        public int _Id { get; set; }
        public string Description { get; set; }
    }
    public class DocumentoResponse
    {
        public int _Id { get; set; }
        public string Document { get; set; }
        public string MaskedDocument { get; set; }
    }

    public class TipoAcessoResponse
    {
        public int _Id { get; set; }
        public string Description { get; set; }
        public bool MainAccess { get; set; }
        public bool FirstAccess{ get; set; }
        public DateTime? LastAccess { get; set; }
        public DateTime? InactivationAccessDate { get; set; }
        public DateTime SignUpDate { get; set; }
    }
}
