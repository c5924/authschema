﻿using AuthSchema.Application.DTO.Request.Identificadores;
using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;
using Xunit.Abstractions;

namespace AuthSchema.WebApi.Model.Request
{
    public class EditaChaveAcessoUsuarioRequest
    {
        [Required(ErrorMessage = "Usuario é Obrigatorio")]
        [Display(Name = "Usuario")]
        public IdentificadorUsuarioRequest Usuario { get; set; }

        [Required(ErrorMessage = "Produto é Obrigatorio")]
        [Display(Name = "Produto")]
        public IdentificadorProdutoRequest Produto { get; set; }

        [Display(Name = "Ativo")]
        public bool Ativo { get; set; }
        [Display(Name = "Bloqueado")]
        public bool Bloqueado { get; set; }

    }
}
