﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AuthSchema.WebApi.Model.Request
{
    public class LoginRequest
    {
        [JsonProperty("Key")]
        [Required(ErrorMessage ="Key is invalid")]
        [MaxLength(255, ErrorMessage ="The size is more then 255 characteres")]
        public string Key { get; set; }

        [JsonProperty("Password")]
        [Required(ErrorMessage = "Password is invalid")]
        [MaxLength(1024, ErrorMessage = "The size is more then 1024 characteres")]
        public string Password { get; set; }

        [JsonProperty("Controle")]
        [Required(ErrorMessage = "Controle is invalid")]
        public ControleRequest Controle { get; set; }
    }
}
