﻿namespace AuthSchema.WebApi.Model.Request
{
    public class RefreshTokenRequest
    {
        public string Key { get; set; }
    }
}
