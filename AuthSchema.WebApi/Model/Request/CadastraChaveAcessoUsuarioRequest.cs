﻿using AuthSchema.Application.DTO.Request.Identificadores;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AuthSchema.WebApi.Model.Request
{
    public class CadastraChaveAcessoUsuarioRequest
    {
        [Required(ErrorMessage = "Usuario é Obrigatorio")]
        [Display(Name = "Usuario")]
        public IdentificadorUsuarioRequest Usuario { get; set; }

        [Required(ErrorMessage = "Produto é Obrigatorio")]
        [Display(Name = "Produto")]
        public IdentificadorProdutoRequest Produto { get; set; }

        [Required(ErrorMessage = "Password é um campo obrigatorio")]
        [Display(Name = "Senha do Usuário")]
        [MaxLength(200, ErrorMessage = "Campo com tamanho máximo de 200 caracteres")]
        public string Password { get; set; }
    }
}
