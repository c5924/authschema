﻿using AuthSchema.Application.DTO.Request.Identificadores;
using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;
using Xunit.Abstractions;

namespace AuthSchema.WebApi.Model.Request
{
    public class AlteraSenhaUsuarioRequest
    {
        [Required(ErrorMessage = "Usuario é Obrigatorio")]
        [Display(Name = "Usuario")]
        public IdentificadorUsuarioRequest Usuario { get; set; }
        [Required(ErrorMessage = "Produto é Obrigatorio")]
        [Display(Name = "Produto")]
        public IdentificadorProdutoRequest Produto { get; set; }
        [Required(ErrorMessage = "NovaSenha é um campo obrigatorio")]
        [Display(Name = "Senha do Usuário")]
        [MaxLength(200, ErrorMessage = "Campo com tamanho máximo de 200 caracteres")]
        public string NovaSenha{ get; set; }
    }
}
