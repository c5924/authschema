﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AuthSchema.WebApi.Model.Request
{
    public class NovoUsuarioRequest
    {
        [Required(ErrorMessage ="NomeUsuario é Obrigatorio")]
        [Display(Name ="Nome do Usuário")]
        [MaxLength(200, ErrorMessage ="Campo com tamanho máximo de 200 caracteres")]
        public string NomeUsuario { get; set; }

        [Required(ErrorMessage ="Email é Obrigatorio")]
        [Display(Name ="Email do Usuário")]
        [RegularExpression(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", ErrorMessage ="Email inválido")]
        [MaxLength(120, ErrorMessage ="Campo com tamanho máximo de 120 caracteres")]
        public string Email { get; set; }
    }
}
