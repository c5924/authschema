﻿using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
using System.Linq;

namespace AuthSchema.WebApi.HostService
{
    public static class KestrelServerOptionsExtensions
    {

        public static void ConfigureEndpoints(this KestrelServerOptions options)
        {
            IConfiguration requiredServices = options.ApplicationServices.GetRequiredService<IConfiguration>();
            IHostingEnvironment environment = options.ApplicationServices.GetRequiredService<IHostingEnvironment>();

            foreach (KeyValuePair<string, EndpointConfiguration> item in from x in requiredServices.GetSection("HttpServer:Endpoints").GetChildren().ToDictionary((IConfigurationSection section) => section.Key, delegate (IConfigurationSection section)
            {
                EndpointConfiguration endpointConfiguration = new EndpointConfiguration();
                section.Bind(endpointConfiguration);
                return endpointConfiguration;
            })
                                                                         where x.Value.Enabled
                                                                         select x)
            {
                EndpointConfiguration config = item.Value;
                int port = config.Port ?? ((config.Scheme == "https") ? 443 : 80);
                List<IPAddress> list = new List<IPAddress>();
                IPAddress address;
                if (config.Host == "localhost" || config.Host == Environment.MachineName)
                {
                    list.Add(IPAddress.IPv6Loopback);
                    list.Add(IPAddress.Loopback);
                }
                else if (IPAddress.TryParse(config.Host, out address))
                {
                    list.Add(address);
                }
                else
                {
                    list.Add(IPAddress.IPv6Any);
                }
                foreach (IPAddress item2 in list)
                {
                    options.Listen(item2, port, delegate (ListenOptions listenOptions)
                    {
                        if (config.Scheme == "https")
                        {

                            listenOptions.UseHttps(LoadCertificate(config, environment));
                        }
                    });
                }
            };
        }

        private static X509Certificate2 LoadCertificate(EndpointConfiguration config, IHostingEnvironment environment)
        {
            if (config.StoreName != null && config.StoreLocation != null)
            {
                using (var x509Store = new X509Store(config.StoreName, Enum.Parse<StoreLocation>(config.StoreLocation)))
                {
                    x509Store.Open(OpenFlags.ReadOnly);
                    var certificateCollection = x509Store.Certificates.Find(X509FindType.FindBySubjectName, config.SubjectName, !environment.IsDevelopment());
                    if (certificateCollection.Count == 0)
                    {
                        throw new InvalidOperationException("Certificado nao encontrado para " + config.SubjectName + ".");
                    }

                    return certificateCollection[0];
                }
            }

            if (config.FilePath != null && config.Password != null)
            {
                return new X509Certificate2(config.FilePath, config.Password);
            }

            throw new InvalidOperationException("Nenhuma configuração válida de certificado encontrada para o endpoint atual");

        }
    }
}
