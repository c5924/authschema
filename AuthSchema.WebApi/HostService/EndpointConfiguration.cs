﻿namespace AuthSchema.WebApi.HostService
{
    public class EndpointConfiguration
    {
        public string Host { get; set; }
        public int? Port { get; set; }
        public string Scheme { get; set; }
        public string SubjectName { get; set; }
        public string StoreName { get; set; }
        public string StoreLocation { get; set; }
        public bool Enabled { get; set; }
        public string FilePath { get; set; }
        public string Password { get; set; }

    }
}
