using AuthSchema.Application.Service;
using AuthSchema.Application.Service.Factories;
using AuthSchema.Application.Service.LoginStrategy;
using AuthSchema.Domain.Model;
using AuthSchema.Domain.Repository;
using AuthSchema.Domain.UOW;
using AuthSchema.Infrastructure.Data.Context;
using AuthSchema.Infrastructure.Data.Providers;
using AuthSchema.Infrastructure.Data.Repository;
using AuthSchema.Infrastructure.Data.UOW;
using AuthSchema.Infrastructure.EntityMapping.Mapping;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthSchema.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();

            //LoginStrategy
            services.AddTransient<Func<Type, ILoginStrategy>>(serviceProvider => type =>
            {
                var loginStrategies = serviceProvider.GetServices<ILoginStrategy>();
                var loginStrategy = loginStrategies.First(_ => _.GetType().BaseType.GenericTypeArguments.Single(x => x.IsAssignableFrom(typeof(User))) != null);
                return loginStrategy;
            });

            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new AutoMapperProfile());
            });

            IMapper mapper = mapperConfig.CreateMapper();
            services.AddSingleton(mapper);

            services.AddTransient<ILoginStrategyFactory, LoginStrategyFactory>();

            services.AddTransient<ILoginProvider, LoginProvider>();

            services.AddTransient<RepositoryProvider>();

            //Context 
            services.AddTransient<IDapperContext, DapperContext>();

            //Unit Of Work
            services.AddTransient<IUnitOfWorkFactory, UnitOfWorkFactory>();
            services.AddTransient<IUsuarioServiceUnifOfWork, UsuarioServiceUnifOfWork>();
            services.AddTransient<IAccessKeyUnitOfWork, AccessKeyUnitOfWork>();
            services.AddTransient<ICardapioVirtualUnitOfWork, CardapioVirtualUnitOfWork>();
            services.AddTransient<IUnitOfWork<IDapperContext>, UnitOfWork>();

            //Login
            services.AddTransient<ILoginStrategy, UserLogin>();

            //Services
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IAccessKeyService, AccessKeyService>();

            //Repositories
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IProductRepository, ProductRepository>();
            services.AddTransient<IAccessKeyRepository, AccessKeyRepository>();
            services.AddTransient<ICardapioVirtualRepository, CardapioVirtualRepository>();

            var key = Encoding.ASCII.GetBytes("fweonisovubqerobvjqdfbv�kqberivb");
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };

            });

            services.AddControllers();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "AuthSchema.WebApi", Version = "v1" });
            });

            services.AddCors();

            services.AddApiVersioning();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "AuthSchema.WebApi v1"));

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();

            app.UseCors(opt => opt.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
