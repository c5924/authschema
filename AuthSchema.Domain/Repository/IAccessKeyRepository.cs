﻿using AuthSchema.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthSchema.Domain.Repository
{
    public interface IAccessKeyRepository
    {
        Task<AccessKey> GetAccessKeyAsync(int userId, int productId, bool onlyActive = true);
        Task<AccessKey> AddAccessKeyAsync(AccessKey accessKey);
        Task<bool> UpdateAccessKeyAsync(AccessKey accessKey);
    }
}
