﻿using AuthSchema.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthSchema.Domain.Repository
{
    public interface IUserRepository
    {
        Task<User> GetUserByNameAndEmailAsync(string name, string email);
        Task<User> GetUserByKeyAsync(string key);
        Task<User> AddUserAsync(User user);
        Task<User> GetUserByIdAsync(int userId);
        Task<IEnumerable<Profile>> GetUserProfilesAsync(int userID);
    }
}
