﻿using AuthSchema.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthSchema.Domain.Repository
{
    public interface IProductRepository
    {
        Task<Product> GetProductAsync(int idProduto);
        Task<IEnumerable<Product>> GetProductsAsync(int userId);
    }
}
