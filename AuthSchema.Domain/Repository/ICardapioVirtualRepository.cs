﻿using AuthSchema.Domain.Model;
using AuthSchema.Domain.Model.CardapioVirtual;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthSchema.Domain.Repository
{
    public interface ICardapioVirtualRepository
    {
        Task<IEnumerable<UserCliente>> ConsultarClientesUsuarioAsync(int userId);
        Task<IEnumerable<UserClienteDeposito>> ConsultarDepositosUsuarioAsync(int IdUsuario);
        Task<Cliente> ConsultarClientesAsync(int idCliente);
        Task<TipoAcesso> ConsultarTipoAcessoAsync(int idTipoAcesso);
        Task<TipoPessoa> ConsultarTipoPessoaAsync(int idTipoPessoa);
    }
}
