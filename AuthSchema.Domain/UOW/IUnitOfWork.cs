﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthSchema.Domain.UOW
{
    public interface IUnitOfWork<TContext> : IDisposable where TContext : class
    {
        TContext Context { get; }
        DbTransaction Transaction { get; }
        void Open();
        void BeginTransaction();
        void Commit();
        void Rollback();
    }
}
