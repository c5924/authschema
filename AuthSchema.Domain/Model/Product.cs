﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthSchema.Domain.Model
{
    public class Product
    {
        public int IdProduto { get; protected set; }
        public string Descricao { get; protected set; }
        public string Nome { get; protected set; }
        public bool Ativo { get; protected set; }

        public Product()
        {

        }
        public Product(int idProduto, string descricao, string nome, bool ativo)
        {
            IdProduto = idProduto;
            Descricao = descricao;
            Nome = nome;
            Ativo = ativo;
        }
    }
}
