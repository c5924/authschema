﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthSchema.Domain.Model.CardapioVirtual
{
    public class TipoAcesso
    {
        public int IdTipoAcesso { get; protected set; }
        public string Descricao { get; protected set; }

        public TipoAcesso()
        {
                
        }

        public TipoAcesso(int idTipoAcesso, string descricao)
        {
            Descricao = descricao;
            IdTipoAcesso = idTipoAcesso;
        }
    }
}
