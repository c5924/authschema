﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthSchema.Domain.Model.CardapioVirtual
{
    public class Cliente
    {
        public int IdCliente { get; protected set; }
        public string Nome { get; protected set; }
        public string Inscricao { get; protected set; }
        public int IdTipoPessoa { get; protected set; }
        public bool Ativo { get; protected set; }
        public string Logo { get; protected set; }
        public int UsuarioInclusao { get; protected set; }
        public DateTime DataInclusao { get; protected set; }
        public int UsuarioAlteracao { get; protected set; }
        public DateTime? DataAlteracao { get; protected set; }
        public IEnumerable<UserClienteDeposito> Depositos { get; protected set; }

        public Cliente()
        {
                
        }
        public Cliente(int idCliente, string nome, string inscricao, int idTipoPessoa, bool ativo, string logo, int usuarioInclusao, DateTime dataInclusao, int usuarioAlteracao, DateTime? dataAlteracao)
        {
            IdCliente = idCliente;
            Nome = nome;
            Inscricao = inscricao;
            IdTipoPessoa = idTipoPessoa;
            Ativo = ativo;
            Logo = logo;
            UsuarioInclusao = usuarioInclusao;
            DataInclusao = dataInclusao;
            UsuarioAlteracao = usuarioAlteracao;
            DataAlteracao = dataAlteracao;
        }


        public void AdicionaDeposito(IEnumerable<UserClienteDeposito> depositos) =>
            Depositos = depositos;
    }
}
