﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthSchema.Domain.Model.CardapioVirtual
{
    public  class UserCliente
    {
        public int IdUsuarioCliente { get; protected set; }
        public int IdUsuario { get; protected set; }
        public int IdCliente { get; protected set; }
        public bool IndAcessoPrincipal { get; protected set; }
        public bool IndPrimeiroAcesso { get; protected set; }
        public int IdTipoAcesso { get; protected set; }
        public DateTime? DataUltimoAcesso { get; protected set; }
        public DateTime? DataInativacaoAcesso { get; protected set; }
        public DateTime DataCadastro { get; protected set; }

        public UserCliente()
        {
                
        }

        public UserCliente(int idUsusarioCliente, int idUsuario, int idCliente, bool indAcessoPrincipal, bool indPrimeiroAcesso, int idTipoAcesso, DateTime? dataUltimoAcesso, DateTime? dataInativacaoAcesso, DateTime dataCadastro)
        {
            IdUsuarioCliente = idUsusarioCliente;
            IdUsuario = idUsuario;
            IdCliente = idCliente;
            IndAcessoPrincipal = indAcessoPrincipal;
            IndPrimeiroAcesso = indPrimeiroAcesso;
            IdTipoAcesso = idTipoAcesso;
            DataUltimoAcesso = dataUltimoAcesso;
            DataInativacaoAcesso = dataInativacaoAcesso;
            DataCadastro = dataCadastro;
        }

    }
}
