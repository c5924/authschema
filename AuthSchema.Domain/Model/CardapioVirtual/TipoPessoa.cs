﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthSchema.Domain.Model.CardapioVirtual
{
    public class TipoPessoa
    {
        public int IdTipoPessoa { get; protected set; }
        public string Descricao { get; protected set; }

        public TipoPessoa()
        {
            
        }

        public TipoPessoa(int idTipoPessoa, string descricao)
        {
            IdTipoPessoa = idTipoPessoa;
            Descricao = descricao;
        }
    }
}
