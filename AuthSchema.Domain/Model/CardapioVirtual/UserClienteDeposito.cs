﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthSchema.Domain.Model.CardapioVirtual
{
    public class UserClienteDeposito
    {
        public int IdUsuarioClienteDeposito { get; set; }
        public int IdCliente { get; set; }
        public int IdUsuarioCliente { get; set; }
        public int IdDeposito { get; set; }
        public bool UltimoAcesso { get; set; }
        public DateTime? DataUltimoAcesso { get; set; }
        public int IdTipoAcesso { get; set; }
    }
}
