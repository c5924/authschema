﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthSchema.Domain.Model
{
    public class AccessKey
    {
        public int IdAccessKey { get; protected set;}
        public string AccessKeyName { get; protected set;}
        public string AccessKeyAttr { get; protected set;}
        public bool Active { get; protected set;}
        public bool Blocked { get; protected set;}
        public DateTime CreationDate { get; protected set;}
        public DateTime? UpdatedDate { get; protected set;}
        public int ProductId { get; protected set;}
        public int UserID { get; protected set;}

        public AccessKey()
        {

        }

        public AccessKey(string accessKeyName, string accessKeyAttr, bool active, bool blocked, DateTime creationDate, DateTime? updatedDate, int productId, int userID)
        {
            AccessKeyName = accessKeyName;
            AccessKeyAttr = accessKeyAttr;
            Active = active;
            Blocked = blocked;
            CreationDate = creationDate;
            UpdatedDate = updatedDate;
            ProductId = productId;
            UserID = userID;
        }

        public AccessKey(int idAccessKey, string accessKeyName, string accessKeyAttr, bool active, bool blocked, DateTime creationDate, DateTime? updatedDate, int productId, int userID)
        {
            IdAccessKey = idAccessKey;
            AccessKeyName = accessKeyName;
            AccessKeyAttr = accessKeyAttr;
            Active = active;
            Blocked = blocked;
            CreationDate = creationDate;
            UpdatedDate = updatedDate;
            ProductId = productId;
            UserID = userID;
        }
    }
}
