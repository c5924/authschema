﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthSchema.Domain.Model
{
    public class UserProfile
    {
        public int IdUsuarioPerfil { get; protected set; }
        public int IdPerfil { get; protected set; }
        public int IdUsuario { get; protected set; }
        public int DataCriacao { get; protected set; }
        public int UsuarioCriacao { get; protected set; }
        public Profile Perfil { get; protected set; }


        public UserProfile()
        {

        }

        public UserProfile(int idUsuarioPerfil, int idPerfil, int idUsuario, int dataCriacao, int usuarioCriacao)
        {
            IdUsuarioPerfil = idUsuarioPerfil;
            IdPerfil = idPerfil;
            IdUsuario = idUsuario;
            DataCriacao = dataCriacao;
            UsuarioCriacao = usuarioCriacao;
        }

        public void carregaPerfil(Profile perfil)
        {
            Perfil = perfil;
        }
    }
}
