﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthSchema.Domain.Model
{
    public class Profile
    {
        public int IdPerfil { get; protected set; }
        public string Descricao { get; protected set; }
        public string Sigla { get; protected set; }
        public int Idproduto { get; protected set; }

        public Profile()
        {

        }

        public Profile(int idPerfil, string descricao, string sigla, int idproduto)
        {
            IdPerfil = idPerfil;
            Descricao = descricao;
            Sigla = sigla;
            Idproduto = idproduto;
        }
    }
}
