﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthSchema.Domain.Model
{
    public class User
    {
        [Key]
        public int IdUsuario { get; protected set; }
        public string Name { get; protected set; }
        public string Email { get; protected set; }

        public User()
        {

        }
        public User(string name, string email)
        {
            Name = name;
            Email = email;
        }
        public User(int idUsuario, string name, string email)
        {
            IdUsuario = idUsuario;
            Name = name;
            Email = email;
        }
    }
}
