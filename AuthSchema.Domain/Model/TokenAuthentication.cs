﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthSchema.Domain.Model
{
    public class TokenAuthentication
    {
        public TokenAuthentication(string token, DateTime expirationDate, DateTime creationDate)
        {
            Token = token;
            ExpirationDate = expirationDate;
            CreationDate = creationDate;
        }
        public string Token{ get; protected set; }
        public DateTime ExpirationDate { get; protected set; }
        public DateTime CreationDate { get; protected set; }
    }
}
