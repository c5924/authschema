﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthSchema.Infrastructure.Data.DTO.CardapioVirtual
{
    public class UserClienteDepositoDTO
    {
        public int IdUsuarioClienteDeposito { get; set; }
        public int IdUsuarioCliente { get; set; }
        public int IdCliente { get; set; }
        public int IdDeposito { get; set; }
        public bool UltimoAcesso { get; set; }
        public DateTime? DataUltimoAcesso { get; set; }
        public int IdTipoAcesso { get; set; }
    }
}
